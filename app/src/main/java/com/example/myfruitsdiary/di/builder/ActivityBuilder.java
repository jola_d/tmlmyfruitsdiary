package com.example.myfruitsdiary.di.builder;

import com.example.myfruitsdiary.ui.main.MainActivity;
import com.example.myfruitsdiary.ui.dialog.AddFruitProvider;
import com.example.myfruitsdiary.ui.fruitdetails.FruitDetailsFragmentProvider;
import com.example.myfruitsdiary.ui.mydiary.EntriesFragmentProvider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {
            EntriesFragmentProvider.class,
            FruitDetailsFragmentProvider.class,
            AddFruitProvider.class
    })
    abstract MainActivity mainActivity();
}
