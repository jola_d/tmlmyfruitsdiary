package com.example.myfruitsdiary.di.component;

import android.app.Application;

import com.example.myfruitsdiary.MyFruitsDiaryApplication;
import com.example.myfruitsdiary.di.builder.ActivityBuilder;
import com.example.myfruitsdiary.di.module.MyFruitAppApplicationModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {MyFruitAppApplicationModule.class,
        AndroidInjectionModule.class,
        ActivityBuilder.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

    void inject(MyFruitsDiaryApplication logisticApplication);
}
