package com.example.myfruitsdiary.di.module;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.example.myfruitsdiary.data.local.db.AppDbHelper;
import com.example.myfruitsdiary.data.local.db.DBHelper;
import com.example.myfruitsdiary.data.local.db.MyDiaryDatabase;
import com.example.myfruitsdiary.di.DataBaseInfo;
import com.example.myfruitsdiary.di.SharedPrefInfo;
import com.example.myfruitsdiary.utils.Constants;
import com.example.myfruitsdiary.data.DataManager;
import com.example.myfruitsdiary.data.MyDiaryDataManager;
import com.example.myfruitsdiary.data.remote.MyDiaryNetworkInterface;
import com.example.myfruitsdiary.data.remote.UriInterceptor;
import com.example.myfruitsdiary.utils.LocalDateTimeConverter;
import com.example.myfruitsdiary.utils.rxschedulers.AppSchedulerProvider;
import com.example.myfruitsdiary.utils.rxschedulers.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class MyFruitAppApplicationModule {


    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    DBHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    MyDiaryDatabase provideAppDatabase(@DataBaseInfo String dbName, Context context) {
        //to update app and not loose data, add Migrations from AppDatabase whenever updating local db version and schema
        return Room.databaseBuilder(context, MyDiaryDatabase.class, dbName).addMigrations(MyDiaryDatabase.MIGRATION).build();
    }

    @Provides
    @Singleton
    DataManager provideDataManager(MyDiaryDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @SharedPrefInfo
    String providePreferenceName() {
        return Constants.PREF_NAME;
    }

    @Provides
    @DataBaseInfo
    String provideDatabaseName() {
        return Constants.DB_NAME + ".db";
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().registerTypeAdapter(Calendar.class, LocalDateTimeConverter.calendarDeserializer).create();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor(UriInterceptor interceptor) {
        return interceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(UriInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit.Builder provideRetrofit(Gson gson, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @Provides
    @Singleton
    MyDiaryNetworkInterface provideRestService(Retrofit.Builder retrofit) {
        return retrofit.build().create(MyDiaryNetworkInterface.class);
    }
}
