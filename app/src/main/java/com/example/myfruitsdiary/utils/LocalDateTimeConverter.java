package com.example.myfruitsdiary.utils;

import android.text.TextUtils;

import androidx.room.TypeConverter;

import com.google.gson.JsonDeserializer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class LocalDateTimeConverter {
    private static final String pattern1 = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final String pattern2 = "yyyy-MM-dd'T'HH:mm:ss.SS";
    private static final String pattern3 = "yyyy-MM-dd HH:mm:ss";
    private static final String pattern4 = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String pattern5 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String pattern6 = "MMMM dd, yyyy HH:mm:ss";

    public static JsonDeserializer<Calendar> calendarDeserializer = (json, typeOfT, context) -> {
        Calendar defaultCalendar = Calendar.getInstance();
        defaultCalendar.set(1970, 1, 1, 0, 0, 0);

        try {
            String dateString = json.getAsJsonPrimitive().getAsString();
            try {
                if (TextUtils.isEmpty(dateString)) {
                    return defaultCalendar;
                }
                return stringToCalendar(dateString, pattern1);
            } catch (ParseException e) {
                try {
                    return stringToCalendar(dateString, pattern2);
                } catch (ParseException e1) {
                    try {
                        return stringToCalendar(dateString, pattern3);
                    } catch (ParseException e2) {
                        try {
                            return stringToCalendar(dateString, pattern4);
                        } catch (ParseException e3) {
                            try {
                                return stringToCalendar(dateString, pattern5);
                            } catch (ParseException e4) {
                                try {
                                    return stringToCalendar(dateString, pattern6);
                                } catch (ParseException e5) {
                                    return defaultCalendar;
                                }
                            }
                        }
                    }
                }
            }
        } catch (IllegalStateException exception) {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Integer.parseInt(json.getAsJsonObject().get("year").toString())
                    , Integer.parseInt(json.getAsJsonObject().get("month").toString())
                    , Integer.parseInt(json.getAsJsonObject().get("dayOfMonth").toString())
                    , Integer.parseInt(json.getAsJsonObject().get("hourOfDay").toString())
                    , Integer.parseInt(json.getAsJsonObject().get("minute").toString())
                    , Integer.parseInt(json.getAsJsonObject().get("second").toString()));
            return calendar1;
        } catch (Exception ex) {
            return defaultCalendar;
        }
    };

    @TypeConverter
    public static String calendarToString(Calendar calendar) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        return format1.format(calendar.getTime());
    }

    @TypeConverter
    public static Calendar stringToCalendar(String calendarText) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        try {
            calendar.setTime(Objects.requireNonNull(format1.parse(calendarText)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    private static Calendar stringToCalendar(String date, String format) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat(format, Locale.US);
        calendar.setTime(Objects.requireNonNull(format1.parse(date)));
        return calendar;
    }
}
