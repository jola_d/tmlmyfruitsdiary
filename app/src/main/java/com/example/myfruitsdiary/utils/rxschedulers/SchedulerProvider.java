package com.example.myfruitsdiary.utils.rxschedulers;

import io.reactivex.Scheduler;

public interface SchedulerProvider {

    Scheduler io();

    Scheduler ui();

}
