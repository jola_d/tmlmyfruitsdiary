package com.example.myfruitsdiary.utils;

public class Constants {

    public static final int NO_INTERNET_CONNECTION = 1;
    public static final int SERVER_ERROR = 2;
    public static final String BASE_URL="https://fruitdiary.test.themobilelife.com/";
    public static final String STANDARD_DATE_FORMAT_TO_DAY = "dd/MM/yyyy HH:mm";
    public static final String STANDARD_DATE_FORMAT= "yyyy-MM-dd";
    public static final String DB_NAME = "myfruiddiary";
    public static final String PREF_NAME = "pref";


}
