package com.example.myfruitsdiary.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.example.myfruitsdiary.utils.Constants.STANDARD_DATE_FORMAT;
import static com.example.myfruitsdiary.utils.Constants.STANDARD_DATE_FORMAT_TO_DAY;

public class CommonUtils {

    public static String calendarToString(Calendar calendar) {
        if (calendar != null) {
            SimpleDateFormat format1 = new SimpleDateFormat(STANDARD_DATE_FORMAT, Locale.US);
            return format1.format(calendar.getTime());
        } else {
            SimpleDateFormat format1 = new SimpleDateFormat(STANDARD_DATE_FORMAT, Locale.US);
            return format1.format(Calendar.getInstance().getTime());
        }
    }
    public static GregorianCalendar longToCalendar(long timeInMillis) {
        if (timeInMillis == -1)
            return null;
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTimeInMillis(timeInMillis);
        return cal;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Network network = cm.getActiveNetwork();
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(network);
                if (capabilities != null){
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
                    return true;
                }else {
                    return false;
                }
            } else {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                return activeNetwork != null &&cm.getActiveNetworkInfo().isConnected();
            }
        }
        return false;
    }

    public static int parseInt(String string) {
        try {
            return Integer.parseInt(string);
        } catch (Exception e) {
            return 0;
        }
    }
}
