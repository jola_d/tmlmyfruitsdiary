package com.example.myfruitsdiary.utils;

import androidx.annotation.Keep;
import androidx.annotation.Nullable;

import java.io.IOException;

@Keep
public class NoConnectivityException extends IOException {

    @Nullable
    @Override
    public String getMessage() {
        return "No connectivity exception";
    }
}
