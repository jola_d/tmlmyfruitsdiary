package com.example.myfruitsdiary.ui.fruitdetails;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.databinding.EmptylistBinding;
import com.example.myfruitsdiary.databinding.ItemRecyclerEntryBinding;
import com.example.myfruitsdiary.databinding.ItemRecyclerFruitBinding;
import com.example.myfruitsdiary.ui.basecomponents.BaseViewHolder;

import java.util.List;

public class FruitsListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_LINE = 1;
    private static final int VIEW_TYPE_EMPTY = 0;

    private List<Fruits> mfruits;

    public FruitsListAdapter(List<Fruits> mLines) {
        this.mfruits = mLines;
    }

    public void addItems(List<Fruits> entryLines) {
        clearItems();
        mfruits.addAll(entryLines);
        notifyDataSetChanged();
    }

    private void clearItems() {
        mfruits.clear();
    }

    @Override
    public int getItemCount() {
        if (mfruits != null && mfruits.size() > 0) {
            return mfruits.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mfruits.size() > 0) {
            return VIEW_TYPE_LINE;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LINE) {
            ItemRecyclerFruitBinding itemBinding = ItemRecyclerFruitBinding.inflate(LayoutInflater.from(parent.getContext()),
                    parent, false);
            return new FruitsViewHolder(itemBinding);
        } else {
            EmptylistBinding emptyBinding = EmptylistBinding.inflate(LayoutInflater.from(parent.getContext()),
                    parent, false);
            return new EmptyViewHolder(emptyBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    public class FruitsViewHolder extends BaseViewHolder {

        ItemRecyclerFruitBinding view;

        FruitsViewHolder(ItemRecyclerFruitBinding itemView) {
            super(itemView.getRoot());
            view = itemView;
        }

        @Override
        public void onBind(int position) {
            final Fruits line = mfruits.get(position);
            view.setFruits(line);
            view.executePendingBindings();
        }
    }

    public static class EmptyViewHolder extends BaseViewHolder {

        EmptylistBinding binding;

        EmptyViewHolder(EmptylistBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        @Override
        public void onBind(int position) {
        }
    }
}