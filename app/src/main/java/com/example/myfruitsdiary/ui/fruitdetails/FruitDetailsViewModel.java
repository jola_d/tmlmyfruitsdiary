package com.example.myfruitsdiary.ui.fruitdetails;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LiveData;

import com.example.myfruitsdiary.data.DataManager;
import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.data.model.FruitsApi;
import com.example.myfruitsdiary.ui.basecomponents.BaseViewModel;
import com.example.myfruitsdiary.utils.rxschedulers.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.Single;

public class FruitDetailsViewModel extends BaseViewModel<FruitDetailsNavigator> {
    public FruitDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public ObservableBoolean showProgress = getIsLoading();


    public void insertFruits(int noOfFruits, int entryId, int fruitId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().insertNewFruit(String.valueOf(entryId), String.valueOf(fruitId), String.valueOf(noOfFruits))
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(aBoolean -> {
                    retriveDataForEntry(entryId);
                }, throwable -> {
                    setIsLoading(false);
                }));
    }

    public void retriveDataForEntry(int entryId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().getAllEntries()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(items -> {
                    for (Entries entries : items) {
                        if (entries.getId() == entryId)
                            getFruitsById(entries.getFruits());
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    setIsLoading(false);
                }));
    }

    public void getFruitsById(ArrayList<FruitsApi> fruits) {

        List<Integer> ids = new ArrayList<>();
        for (FruitsApi api : fruits) {
            ids.add(api.getFruitId());
        }
        getCompositeDisposable().add(getDataManager().getFruitsById(ids)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(fruits1 -> {
                    getNavigator().addItemsToAdapter(new ArrayList<>(fruits1));
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    throwable.printStackTrace();
                }));


    }
}
