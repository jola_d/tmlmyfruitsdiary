package com.example.myfruitsdiary.ui.main;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.myfruitsdiary.R;
import com.example.myfruitsdiary.ViewModelProviderFactory;
import com.example.myfruitsdiary.databinding.ActivityMainBinding;
import com.example.myfruitsdiary.ui.AboutFragment;
import com.example.myfruitsdiary.ui.basecomponents.BaseActivity;
import com.example.myfruitsdiary.ui.fruitdetails.FruitDetailsFragment;
import com.example.myfruitsdiary.ui.mydiary.EntriesFragment;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainActivityListener
        , FragmentManager.OnBackStackChangedListener
        , HasSupportFragmentInjector {

    @Inject
    ViewModelProviderFactory factory;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        return new ViewModelProvider(this, factory).get(MainViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getViewModel().setNavigator(this);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        addFragment(new EntriesFragment());
    }

    @Override
    public void onBackStackChanged() {

    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_frame, fragment);
        transaction.commit();
    }


    @Override
    public void openFruitDetails(int entryId) {
        FruitDetailsFragment fragment = new FruitDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("entryId", entryId);
        fragment.setArguments(args);
        loadFragment(fragment);
    }

    @Override
    public void updateAdapter() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_frame);
        if (currentFragment instanceof EntriesFragment) {
            ((EntriesFragment) currentFragment).getViewModel().itemsRemoved();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.delete).setVisible(getSupportFragmentManager().findFragmentById(R.id.main_frame) instanceof EntriesFragment);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_menu: {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_frame, new AboutFragment());
                if (getSupportFragmentManager().findFragmentById(R.id.main_frame) instanceof EntriesFragment) {
                    transaction.addToBackStack(null);
                }
                transaction.commit();
                break;
            }
            case R.id.delete:
                deleteItems();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteItems() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_delete);
        builder.setPositiveButton(R.string.remove, (dialog, which) -> {
            getViewModel().deleteAllEntries();
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
        });
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

}