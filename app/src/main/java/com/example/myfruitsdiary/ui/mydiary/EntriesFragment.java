package com.example.myfruitsdiary.ui.mydiary;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfruitsdiary.BR;
import com.example.myfruitsdiary.ui.main.MainActivityListener;
import com.example.myfruitsdiary.R;
import com.example.myfruitsdiary.ViewModelProviderFactory;
import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.databinding.FragmentEntriesBinding;
import com.example.myfruitsdiary.ui.basecomponents.BaseFragment;
import com.example.myfruitsdiary.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import javax.inject.Inject;

import static com.example.myfruitsdiary.utils.Constants.NO_INTERNET_CONNECTION;
import static com.example.myfruitsdiary.utils.Constants.SERVER_ERROR;

public class EntriesFragment extends BaseFragment<FragmentEntriesBinding, EntriesViewModel> implements EntriesNavigator, EntriesListAdapter.EntryListItemClick {


    private MainActivityListener mActivityListener;
    @Inject
    ViewModelProviderFactory factory;
    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    EntriesListAdapter mAdapter;

    @Override
    public int getBindingVariable() {
        return BR.entriesViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_entries;
    }

    @Override
    public EntriesViewModel getViewModel() {
        return new ViewModelProvider(this, factory).get(EntriesViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        getViewModel().setNavigator(this);


    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecyclerView();
        loadAllFruits();
        getViewModel().getMutableLiveData().observe(getViewLifecycleOwner(), entries -> {
            addItemsToAdapter((ArrayList<Entries>) entries);
        });
        getViewDataBinding().newEntryFab.setOnClickListener(v -> chooseEntrydate());

    }

    private void loadAllFruits() {
        getViewModel().getFruits().observe(getViewLifecycleOwner(), fruits -> {
            mAdapter.setFruits(fruits);
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mActivityListener = (MainActivityListener) context;
        }
    }

    private void setUpRecyclerView() {

        if (CommonUtils.isNetworkConnected(Objects.requireNonNull(getContext()))) {

            mLayoutManager.setOrientation(RecyclerView.VERTICAL);
            getViewDataBinding().recycler.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
            getViewDataBinding().recycler.setAdapter(mAdapter);
            mAdapter.setListener(this);
            getViewModel().getEntriesData();
            setupSwipeToDelete(getViewDataBinding().recycler);
        } else {
            showErrorMessage(NO_INTERNET_CONNECTION);
        }
    }

    @Override
    public void addItemsToAdapter(ArrayList<Entries> entries) {
        if (mAdapter != null) {
            mAdapter.addItems(entries);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void updateAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void allItemsRemoved() {
        if (mAdapter != null) {
            mAdapter.removeAllItems();
        }
    }

    @Override
    public void showErrorMessage(int error) {
        switch (error) {
            case NO_INTERNET_CONNECTION:
                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                break;
            case SERVER_ERROR:
                Toast.makeText(getContext(), getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                break;

        }
    }

    @Override
    public void chooseEntrydate() {
        // Pop date dialog
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener onDateSetListener = (view1, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(year, monthOfYear, dayOfMonth);
            getViewModel().insertNewEntry(myCalendar);
        };
        new DatePickerDialog(getContext(), onDateSetListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    private void setupSwipeToDelete(RecyclerView recyclerView) {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.confirm_delete);
                    builder.setPositiveButton(R.string.remove, (dialog, which) -> {
                        String entryId = String.valueOf(mAdapter.getEntry(position).getId());
                        getViewModel().deleteEntry(entryId);
                        mAdapter.removeEntry(position);
                    });
                    builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
                        mAdapter.notifyDataSetChanged();
                    });
                    builder.show();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    @Override
    public void onListItemClick(int entryId) {
        mActivityListener.openFruitDetails(entryId);
    }
}
