package com.example.myfruitsdiary.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.myfruitsdiary.BuildConfig;
import com.example.myfruitsdiary.R;
import com.example.myfruitsdiary.databinding.FragmentAboutBinding;
import com.example.myfruitsdiary.ui.basecomponents.BaseFragment;

public class AboutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentAboutBinding bindingaboutFragment = DataBindingUtil.inflate(inflater, R.layout.fragment_about,container,false);
        bindingaboutFragment.setVersion(BuildConfig.VERSION_NAME);
        return  bindingaboutFragment.getRoot();
    }

}
