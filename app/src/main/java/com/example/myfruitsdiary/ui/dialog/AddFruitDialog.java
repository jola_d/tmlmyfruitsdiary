package com.example.myfruitsdiary.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.myfruitsdiary.R;
import com.example.myfruitsdiary.ViewModelProviderFactory;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.databinding.DialogAddFruitBinding;
import com.example.myfruitsdiary.ui.basecomponents.BaseDialog;
import com.example.myfruitsdiary.utils.CommonUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

import static android.content.ContentValues.TAG;

public class AddFruitDialog extends BaseDialog implements AddFruitNavigator {

    AddFruitListener listener;
    AddFruitViewModel dialogViewModel;
    DialogAddFruitBinding dialogAddFruitBinding;

    Fruits fruit;

    private void setDialogListener(AddFruitListener listener) {
        this.listener = listener;
    }

    @Inject
    ViewModelProviderFactory viewModelProviderFactory;

    public static AddFruitDialog newInstance(AddFruitListener listener) {

        AddFruitDialog fruitDialog = new AddFruitDialog();
        fruitDialog.setDialogListener(listener);
        return fruitDialog;

    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dialogAddFruitBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_fruit, container, false);
        View view = dialogAddFruitBinding.getRoot();

        AndroidSupportInjection.inject(this);
        dialogViewModel = new ViewModelProvider(this, viewModelProviderFactory).get(AddFruitViewModel.class);
        dialogAddFruitBinding.setAddFruitViewModel(dialogViewModel);
        dialogViewModel.setNavigator(this);

        setUpViewActions();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel.getAllFruits();
    }

    private void setUpViewActions() {
        dialogAddFruitBinding.okBtn.setOnClickListener(v ->
                dialogViewModel.addFruit(CommonUtils.parseInt(dialogAddFruitBinding.etAddNo.getText().toString()),fruit.getId()));
    }


    @Override
    public boolean cancelOnOutside() {
        return true;
    }

    @Override
    public void dismissDialog() {
        dismissDialog(TAG);
    }

    @Override
    public void setSpinnerAdapter(ArrayList<Fruits> fruits) {
        ArrayAdapter<Fruits> adapter = new ArrayAdapter<Fruits>(getContext(), android.R.layout.simple_list_item_1, fruits);
        dialogAddFruitBinding.spinner.setAdapter(adapter);
        dialogAddFruitBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fruit = (Fruits) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void addFruit(int noOfFruits, int fruitId) {
        if (listener != null) {
            listener.addFruit(noOfFruits,fruitId);
        }
    }

    public interface AddFruitListener {
        void addFruit(int noOfFruits, int fruitId);
    }
}
