package com.example.myfruitsdiary.ui.main;

import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;
import java.util.List;

public interface MainActivityListener {

    void openFruitDetails(int entryId);

    void updateAdapter();

}
