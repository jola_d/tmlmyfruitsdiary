package com.example.myfruitsdiary.ui.fruitdetails;

import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;

public interface FruitDetailsNavigator {

    void addItemsToAdapter(ArrayList<Fruits> fruits);

    void callAddFruitDialog();
}
