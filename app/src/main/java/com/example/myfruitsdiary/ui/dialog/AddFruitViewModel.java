package com.example.myfruitsdiary.ui.dialog;

import com.example.myfruitsdiary.data.DataManager;
import com.example.myfruitsdiary.ui.basecomponents.BaseViewModel;
import com.example.myfruitsdiary.utils.rxschedulers.SchedulerProvider;

import java.util.ArrayList;

public class AddFruitViewModel extends BaseViewModel<AddFruitNavigator> {
    public AddFruitViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }



    public void addFruit(int fruitNo,int fruitId) {
        if (fruitNo > 0) {
            getNavigator().addFruit(fruitNo,fruitId);
        }
        getNavigator().dismissDialog();

    }

    public void getAllFruits() {
        getCompositeDisposable().add(getDataManager().getAllFruits()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(items -> {
                    getNavigator().setSpinnerAdapter(new ArrayList<>(items));
                }, throwable -> {
                    throwable.printStackTrace();
                    setIsLoading(false);
                }));
    }

    public void cancel() {
        getNavigator().dismissDialog();
    }

}
