package com.example.myfruitsdiary.ui.fruitdetails;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfruitsdiary.BR;
import com.example.myfruitsdiary.R;
import com.example.myfruitsdiary.ViewModelProviderFactory;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.databinding.FragmentEntryFruitsBinding;
import com.example.myfruitsdiary.ui.basecomponents.BaseFragment;
import com.example.myfruitsdiary.ui.dialog.AddFruitDialog;

import java.util.ArrayList;

import javax.inject.Inject;

public class FruitDetailsFragment extends BaseFragment<FragmentEntryFruitsBinding, FruitDetailsViewModel> implements FruitDetailsNavigator, AddFruitDialog.AddFruitListener {
    int entryId = 0;
    @Inject
    ViewModelProviderFactory factory;
    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    FruitsListAdapter mAdapter;

    @Override
    public int getBindingVariable() {
        return BR.fruitsDiaryViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_entry_fruits;
    }

    @Override
    public FruitDetailsViewModel getViewModel() {
        return new ViewModelProvider(this, factory).get(FruitDetailsViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewModel().setNavigator(this);
        setUpRecyclerView();
        Bundle bundle = getArguments();

        if (bundle != null) {
            entryId = bundle.getInt("entryId");
        }
        getViewModel().retriveDataForEntry(entryId);
        getViewDataBinding().newFruitFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAddFruitDialog();
            }
        });

    }

    private void setUpRecyclerView() {
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        getViewDataBinding().recycler.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        getViewDataBinding().recycler.setAdapter(mAdapter);
    }

    @Override
    public void addItemsToAdapter(ArrayList<Fruits> entries) {
        if (mAdapter != null) {
            mAdapter.addItems(entries);
        }
    }

    @Override
    public void callAddFruitDialog() {
        AddFruitDialog.newInstance(this).show(getParentFragmentManager());
    }

    @Override
    public void addFruit(int noOfFruits, int fruitId) {
        getViewModel().insertFruits(noOfFruits, entryId,fruitId);

    }
}
