package com.example.myfruitsdiary.ui.dialog;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddFruitProvider {
    @ContributesAndroidInjector
    abstract AddFruitDialog provideDialog();
}
