package com.example.myfruitsdiary.ui.fruitdetails;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class FruitDetailsFragmentModule {

    @Provides
    FruitsListAdapter provideEntriesLineAdapter() {
        ArrayList<Fruits> entries = new ArrayList<>();
        entries.add(new Fruits());
        return new FruitsListAdapter(entries);
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(FruitDetailsFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
