package com.example.myfruitsdiary.ui.main;

import com.example.myfruitsdiary.data.DataManager;
import com.example.myfruitsdiary.ui.basecomponents.BaseViewModel;
import com.example.myfruitsdiary.utils.rxschedulers.SchedulerProvider;

public class MainViewModel extends BaseViewModel<MainActivityListener> {
    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    public void deleteAllEntries() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().deleteAllEntries()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(aBoolean -> {
                    setIsLoading(false);
                    getNavigator().updateAdapter();
                }, throwable -> {
                    setIsLoading(false);
                    throwable.printStackTrace();
                }));
    }


}
