package com.example.myfruitsdiary.ui.mydiary;

import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.data.model.FruitsApi;

import java.util.ArrayList;

public class ItemViewModel {

    Integer entryId;
    ItemViewModelListener listener;

    ItemViewModel(Integer entryId, ItemViewModelListener listener) {
        this.listener = listener;
        this.entryId = entryId;
    }

    public void onItemClick() {
        listener.onItemClick(entryId.intValue());
    }

    public interface ItemViewModelListener {
        void onItemClick(int id);

    }
}
