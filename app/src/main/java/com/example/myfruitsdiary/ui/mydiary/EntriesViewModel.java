package com.example.myfruitsdiary.ui.mydiary;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myfruitsdiary.data.DataManager;
import com.example.myfruitsdiary.data.model.Data;
import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.data.model.FruitsApi;
import com.example.myfruitsdiary.ui.basecomponents.BaseViewModel;
import com.example.myfruitsdiary.utils.CommonUtils;
import com.example.myfruitsdiary.utils.rxschedulers.SchedulerProvider;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

import static com.example.myfruitsdiary.utils.Constants.SERVER_ERROR;

public class EntriesViewModel extends BaseViewModel<EntriesNavigator> {

    public EntriesViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public ObservableBoolean showProgress = getIsLoading();
    private final MutableLiveData<List<Entries>> mutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<Fruits>> mutableLiveDataFruits = new MutableLiveData<>();
    ArrayList<Fruits> fruits;

    public void insertNewEntry(Calendar calendar) {

        getCompositeDisposable().add(getDataManager().insertNewEntry(new Data(CommonUtils.calendarToString(calendar)))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(data -> getEntriesData(), Throwable::printStackTrace));

    }

    public void refreshData() {
        getCompositeDisposable().add(getDataManager().getAllEntries()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(entries -> getNavigator().updateAdapter(), Throwable::printStackTrace));

    }

    public void insertFruits(List<Fruits> fruits) {
        getCompositeDisposable().add(getDataManager().insertFruits(fruits)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(aBoolean -> {
                }, Throwable::printStackTrace));
    }


    public LiveData<List<Entries>> getMutableLiveData() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().getAllEntries()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(items -> {
                    setIsLoading(false);
                    ArrayList<Entries> entriesData = new ArrayList<>();
                    for (Entries entries : items) {
                        entries.setNbrOfFruits(entries.fruits.stream()
                                .mapToInt(entryFruit -> entryFruit.amount)
                                .sum());
                        entriesData.add(entries);
                    }
                    mutableLiveData.postValue(entriesData);
                }, throwable -> {
                    setIsLoading(false);
                    throwable.printStackTrace();
                }));
        return mutableLiveData;
    }

    public LiveData<List<Fruits>> getFruits() {
        getCompositeDisposable().add(getDataManager().getAllFruits()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe((fruits) -> {
                    mutableLiveDataFruits.postValue(fruits);
                    insertFruits(fruits);
                }, Throwable::printStackTrace));
        return mutableLiveDataFruits;
    }

    public void itemsRemoved(){
        getNavigator().allItemsRemoved();
    }

    public void getEntriesData() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().getAllEntries()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(items -> {
                    setIsLoading(false);
                    getNavigator().addItemsToAdapter(items);
                }, throwable -> {
                    getNavigator().showErrorMessage(SERVER_ERROR);
                    setIsLoading(false);
                    throwable.printStackTrace();
                }));
    }


    public void deleteEntry(String id) {
        getCompositeDisposable().add(getDataManager().deleteEntryById(id)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(aBoolean -> {
                    getNavigator().showErrorMessage(4);
                    getNavigator().updateAdapter();
                }, Throwable::printStackTrace));
    }
}
