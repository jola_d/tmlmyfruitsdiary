package com.example.myfruitsdiary.ui.dialog;

import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;

public interface AddFruitNavigator {

    void dismissDialog();

    void setSpinnerAdapter(ArrayList<Fruits> fruits);

    void addFruit(int noOfFruits, int fruitId);
}
