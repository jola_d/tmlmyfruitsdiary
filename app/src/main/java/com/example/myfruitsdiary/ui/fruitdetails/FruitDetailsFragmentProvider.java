package com.example.myfruitsdiary.ui.fruitdetails;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FruitDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = {
            FruitDetailsFragmentModule.class
    })
    abstract FruitDetailsFragment provideHistoryFragment();
}
