package com.example.myfruitsdiary.ui.mydiary;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.myfruitsdiary.data.model.Entries;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class EntriesFragmentModule {

    @Provides
    EntriesListAdapter provideEntriesLineAdapter() {
        ArrayList<Entries> entries = new ArrayList<>();
        entries.add(new Entries());
        return new EntriesListAdapter(entries);
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(EntriesFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
