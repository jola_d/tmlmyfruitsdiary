package com.example.myfruitsdiary.ui.mydiary;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.data.model.FruitsApi;
import com.example.myfruitsdiary.databinding.EmptylistBinding;

import com.example.myfruitsdiary.databinding.ItemRecyclerEntryBinding;
import com.example.myfruitsdiary.ui.basecomponents.BaseViewHolder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class EntriesListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_LINE = 1;
    private static final int VIEW_TYPE_EMPTY = 0;

    private List<Entries> mEntries;
    private EntryListItemClick mListener;
    private Map<Integer, Fruits> fruitList = new HashMap<>();


    public void setListener(EntryListItemClick listener) {
        this.mListener = listener;
    }

    public EntriesListAdapter(List<Entries> mLines) {
        this.mEntries = mLines;
    }

    public void addItems(List<Entries> entryLines) {
        clearItems();
        mEntries.addAll(entryLines);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mEntries.clear();
    }

    public void removeAllItems() {
        int size = mEntries.size();
        clearItems();
        notifyItemRangeRemoved(0, size);
        notifyDataSetChanged();
    }

    public Entries getEntry(int position) {
        return mEntries.get(position);
    }

    public void removeEntry(int position) {
        mEntries.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public int getItemCount() {

        if (mEntries != null && mEntries.size() > 0) {
            return mEntries.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mEntries.size() > 0) {
            return VIEW_TYPE_LINE;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LINE) {
            ItemRecyclerEntryBinding itemBinding = ItemRecyclerEntryBinding.inflate(LayoutInflater.from(parent.getContext()),
                    parent, false);
            return new EntriesViewHolder(itemBinding);
        } else {
            EmptylistBinding emptyBinding = EmptylistBinding.inflate(LayoutInflater.from(parent.getContext()),
                    parent, false);
            return new EmptyViewHolder(emptyBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {


        holder.onBind(position);
    }

    public void setFruits(List<Fruits> fruitsList) {
        fruitsList.forEach(fruit -> fruitList.put(fruit.getId(), fruit));
    }

    public class EntriesViewHolder extends BaseViewHolder implements ItemViewModel.ItemViewModelListener {
        ItemViewModel itemViewModel;
        ItemRecyclerEntryBinding view;

        EntriesViewHolder(ItemRecyclerEntryBinding itemView) {
            super(itemView.getRoot());
            view = itemView;
        }

        @Override
        public void onBind(int position) {
            final Entries line = mEntries.get(position);
            itemViewModel = new ItemViewModel(line.getId(), this);
            view.setItemViewModel(itemViewModel);
            view.setEntries(line);
            view.executePendingBindings();
        }


        @Override
        public void onItemClick(int id) {
            mListener.onListItemClick(id);
        }
    }

    public static class EmptyViewHolder extends BaseViewHolder {

        EmptylistBinding binding;

        EmptyViewHolder(EmptylistBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        @Override
        public void onBind(int position) {

        }

    }

    public interface EntryListItemClick {
        void onListItemClick(int id);
    }

}
