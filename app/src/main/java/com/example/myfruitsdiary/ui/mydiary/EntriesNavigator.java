package com.example.myfruitsdiary.ui.mydiary;

import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;
import java.util.List;

public interface EntriesNavigator {

    void addItemsToAdapter(ArrayList<Entries> entries);

    void updateAdapter();

    void allItemsRemoved();

    void showErrorMessage(int error);

    void chooseEntrydate();


}
