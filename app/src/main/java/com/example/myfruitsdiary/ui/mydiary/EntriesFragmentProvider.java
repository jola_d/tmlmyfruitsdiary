package com.example.myfruitsdiary.ui.mydiary;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class EntriesFragmentProvider {

    @ContributesAndroidInjector(modules = {
            EntriesFragmentModule.class
    })
    abstract EntriesFragment provideHistoryFragment();
}
