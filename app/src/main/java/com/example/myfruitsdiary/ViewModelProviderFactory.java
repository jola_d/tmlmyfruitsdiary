package com.example.myfruitsdiary;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.myfruitsdiary.data.DataManager;
import com.example.myfruitsdiary.ui.dialog.AddFruitViewModel;
import com.example.myfruitsdiary.ui.fruitdetails.FruitDetailsViewModel;
import com.example.myfruitsdiary.ui.main.MainViewModel;
import com.example.myfruitsdiary.ui.mydiary.EntriesViewModel;
import com.example.myfruitsdiary.utils.rxschedulers.SchedulerProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ViewModelProviderFactory extends ViewModelProvider.NewInstanceFactory {


    private final DataManager dataManager;
    private final SchedulerProvider schedulerProvider;

    @Inject
    public ViewModelProviderFactory(DataManager dataManager,
                                    SchedulerProvider schedulerProvider) {
        this.dataManager = dataManager;
        this.schedulerProvider = schedulerProvider;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            //noinspection unchecked
            return (T) new MainViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(EntriesViewModel.class)) {
            //noinspection unchecked
            return (T) new EntriesViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(FruitDetailsViewModel.class)) {
            //noinspection unchecked
            return (T) new FruitDetailsViewModel(dataManager, schedulerProvider);
        }else if (modelClass.isAssignableFrom(AddFruitViewModel.class)) {
            //noinspection unchecked
            return (T) new AddFruitViewModel(dataManager, schedulerProvider);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
