package com.example.myfruitsdiary.data.local.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

@Dao
public interface FruitsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Fruits> fruits);

    @Query("Select vitamins from fruits where id= :id")
    Single<Integer> selectVitaminsFromFruit(int id);

    @Query("Select * from fruits where id in (:id)")
    Observable<List<Fruits>> getFruitById(List<Integer> id);

    @Query("Select * from fruits ")
    Observable<List<Fruits>> getAllFruits();

}
