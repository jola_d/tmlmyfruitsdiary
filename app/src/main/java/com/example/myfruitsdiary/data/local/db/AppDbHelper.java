package com.example.myfruitsdiary.data.local.db;

import com.example.myfruitsdiary.data.model.Fruits;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

@Singleton
public class AppDbHelper implements DBHelper {
    private final MyDiaryDatabase mAppDatabase;

    @Inject
    AppDbHelper(MyDiaryDatabase appDatabase) {
        this.mAppDatabase = appDatabase;
    }


    @Override
    public Observable<List<Fruits>> insertFruits(List<Fruits> line) {
        return Observable.fromCallable(() -> {
            mAppDatabase.getFruitsLineDao().insert(line);
            return line;
        });
    }

    @Override
    public Single<Integer> getVitaminsFromFruit(Integer id) {
        return mAppDatabase.getFruitsLineDao().selectVitaminsFromFruit(id);
    }

    @Override
    public Observable<List<Fruits>> getFruitsById(List<Integer> id) {
        return mAppDatabase.getFruitsLineDao().getFruitById(id);
    }

    @Override
    public Observable<List<Fruits>> getAllFruitsFromDb() {
        return mAppDatabase.getFruitsLineDao().getAllFruits();
    }


}
