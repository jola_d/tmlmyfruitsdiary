package com.example.myfruitsdiary.data.remote;

import android.content.Context;

import com.example.myfruitsdiary.utils.CommonUtils;
import com.example.myfruitsdiary.utils.NoConnectivityException;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Response;

public class UriInterceptor implements Interceptor {
    private Context mContext;

    @Inject
    UriInterceptor( Context context) {
        this.mContext = context;
    }

    private String[] getSchemeAndHost(String url) {
        HttpUrl httpUrl = HttpUrl.parse(url);
        assert httpUrl != null;
        return new String[]{httpUrl.scheme(), httpUrl.host()};
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!CommonUtils.isNetworkConnected(mContext)) {
            throw new NoConnectivityException();
        }
        return chain.proceed(chain.request());
    }
}
