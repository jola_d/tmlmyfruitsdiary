package com.example.myfruitsdiary.data.model;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FruitsApi {
    @Expose
    @SerializedName("fruitId")
    public Integer fruitId;
    @Expose
    @SerializedName("fruitType")
    private String fruitType;
    @Expose
    @SerializedName("amount")
    public Integer amount;

    public FruitsApi(Integer fruitId, String fruitType, Integer amount) {
        this.fruitId = fruitId;
        this.fruitType = fruitType;
        this.amount = amount;
    }

    public Integer getFruitId() {
        return fruitId;
    }

    public void setFruitId(Integer fruitId) {
        this.fruitId = fruitId;
    }

    public String getFruitType() {
        return fruitType;
    }

    public void setFruitType(String fruitType) {
        this.fruitType = fruitType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
