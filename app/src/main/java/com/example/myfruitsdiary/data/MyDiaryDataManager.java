package com.example.myfruitsdiary.data;

import com.example.myfruitsdiary.data.local.db.DBHelper;
import com.example.myfruitsdiary.data.model.Data;
import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.data.remote.MyDiaryNetworkInterface;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;

@Singleton
public class MyDiaryDataManager implements DataManager {


    private final DBHelper mDbHelper;

    private final MyDiaryNetworkInterface mNetworkInterface;

    @Inject
    public MyDiaryDataManager(DBHelper mDbHelper, MyDiaryNetworkInterface mNetworkInterface) {
        this.mDbHelper = mDbHelper;
        this.mNetworkInterface = mNetworkInterface;
    }

    @Override
    public Observable<ArrayList<Entries>> getAllEntries() {
        return mNetworkInterface.getAllEntries();
    }

    @Override
    public Observable<List<Fruits>> getAllFruits() {
        return mNetworkInterface.getAllFruits();
    }



    @Override
    public Observable<ResponseBody> insertNewEntry(Data date) {
        return mNetworkInterface.insertNewEntry(date);
    }

    @Override
    public  Observable<ResponseBody> insertNewFruit(String entryId, String fruitId, String noOfFruits) {
        return mNetworkInterface.insertNewFruit(entryId, fruitId, noOfFruits);
    }

    @Override
    public   Observable<ResponseBody> deleteEntryById(String id) {
        return mNetworkInterface.deleteEntryById(id);
    }

    @Override
    public Observable<Boolean> deleteAllEntries() {
        return mNetworkInterface.deleteAllEntries();
    }

    @Override
    public Observable<List<Fruits>> insertFruits(List<Fruits> line) {
        return mDbHelper.insertFruits(line);
    }

    @Override
    public Single<Integer> getVitaminsFromFruit(Integer id) {
        return mDbHelper.getVitaminsFromFruit(id);
    }

    @Override
    public Observable<List<Fruits>> getFruitsById(List<Integer> id) {
        return mDbHelper.getFruitsById(id);
    }

    @Override
    public Observable<List<Fruits>> getAllFruitsFromDb() {
        return mDbHelper.getAllFruitsFromDb();
    }
}
