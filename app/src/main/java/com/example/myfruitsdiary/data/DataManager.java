package com.example.myfruitsdiary.data;

import com.example.myfruitsdiary.data.local.db.DBHelper;
import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.data.remote.MyDiaryNetworkInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public interface DataManager extends  MyDiaryNetworkInterface, DBHelper {

    Observable<ArrayList<Entries>> getAllEntries();

    Observable<List<Fruits>> getAllFruits();

}
