package com.example.myfruitsdiary.data.local.db;

import com.example.myfruitsdiary.data.model.Data;
import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface DBHelper {

    Observable<List<Fruits>> insertFruits(List<Fruits> line);

    Single<Integer> getVitaminsFromFruit(Integer id);

    Observable<List<Fruits>> getFruitsById(List<Integer> id);

    Observable<List<Fruits>> getAllFruitsFromDb();
}
