package com.example.myfruitsdiary.data.local.db;


import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.myfruitsdiary.data.model.Fruits;
import com.example.myfruitsdiary.utils.LocalDateTimeConverter;

@Database(entities = {
        Fruits.class
}, version = 1)
@TypeConverters({LocalDateTimeConverter.class})
public abstract class MyDiaryDatabase extends RoomDatabase {
    public static final Migration MIGRATION = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `fruits` (`local_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`id` TEXT, `type` TEXT, `vitamins` INTEGER, `image` TEXT)");
        }
    };

    public abstract FruitsDao getFruitsLineDao();

}
