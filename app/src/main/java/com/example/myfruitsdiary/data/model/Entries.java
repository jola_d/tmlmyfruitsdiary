package com.example.myfruitsdiary.data.model;

import android.support.v4.app.INotificationSideChannel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Entries {

    @Expose
    @SerializedName("id")
    private Integer id;
    @Expose
    @SerializedName("date")
    private String date;
    @Expose
    @SerializedName("fruit")
    public ArrayList<FruitsApi> fruits;


    public int nbrOfFruits;

    public int nbrOfVitamins;

    public int getNbrOfFruits() {
        return nbrOfFruits;
    }

    public void setNbrOfFruits(int nbrOfFruits) {
        this.nbrOfFruits = nbrOfFruits;
    }

    public int getNbrOfVitamins() {
        return nbrOfVitamins;
    }

    public void setNbrOfVitamins(int nbrOfVitamins) {
        this.nbrOfVitamins = nbrOfVitamins;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<FruitsApi> getFruits() {
        return fruits;
    }

    public void setFruits(ArrayList<FruitsApi> fruits) {
        this.fruits = fruits;
    }
}
