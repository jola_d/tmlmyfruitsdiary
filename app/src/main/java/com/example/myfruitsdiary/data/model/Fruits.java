package com.example.myfruitsdiary.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.myfruitsdiary.R;
import com.example.myfruitsdiary.utils.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

@Entity(tableName = "fruits")
public class Fruits {


    @Expose
    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @Expose
    @SerializedName("type")
    @ColumnInfo(name = "type")
    private String type;
    @Expose
    @SerializedName("vitamins")
    @ColumnInfo(name = "vitamins")
    public int vitamins;
    @Expose
    @SerializedName("image")
    @ColumnInfo(name = "image")
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVitamins() {
        return vitamins;
    }

    public void setVitamins(int vitamins) {
        this.vitamins = vitamins;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Fruits() {
    }

    public Fruits(Integer id, String type, int vitamins, String image) {
        this.id = id;
        this.type = type;
        this.vitamins = vitamins;
        this.image = image;
    }

    // important code for loading image here
    @BindingAdapter({"image"})
    public static void loadImage(ImageView imageView, String imageURL) {
        Picasso.with(imageView.getContext())
                .load(Constants.BASE_URL + imageURL)
                .placeholder(R.drawable.ic_reload)
                .into(imageView);


    }

    @Override
    public String toString() {
        return "Fruit:" + type +
                ", vitamins: " + vitamins;
    }
}
