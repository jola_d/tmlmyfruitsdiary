package com.example.myfruitsdiary.data.remote;

import com.example.myfruitsdiary.data.model.Data;
import com.example.myfruitsdiary.data.model.Entries;
import com.example.myfruitsdiary.data.model.Fruits;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyDiaryNetworkInterface {

    @GET("api/entries")
    Observable<ArrayList<Entries>> getAllEntries();
    @GET("api/fruit")
    Observable<List<Fruits>> getAllFruits();
    @POST("api/entries")
    Observable<ResponseBody> insertNewEntry(@Body Data date);
    @POST("api/entry/{entryId}/fruit/{fruitId}")
    Observable<ResponseBody> insertNewFruit(
            @Path("entryId") String entryId,
            @Path("fruitId") String fruitId,
            @Query("amount") String noOfFruits);
    @DELETE("api/entry/{entryId}")
    Observable<ResponseBody> deleteEntryById(@Path("entryId") String id);
    @DELETE("api/entries")
    Observable<Boolean> deleteAllEntries();

}
